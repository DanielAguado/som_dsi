#!/usr/bin/python3

import sqlite3
import os
import sys
import datetime
import getopt
import math

INPUTFILENAME = 'train.csv'
DATABASENAME = 'database.db'
OUTPUTFILENAME = 'new_training_dataset_expanded'


def get_create_table_query(tags):
    columns = []
    for t in tags.split(','):
        columns.append("%s text" % t)
    return "create table dataset (id integer, %s);" % ','.join(columns)

def to_string(data):
    line = data.replace("'", "").replace('\n', '').split('"')

    if (len(line) % 2 == 0):
        print ("ERROR!!!! %s" % data)
        sys.exit()

    result = []
    for index, l in enumerate(line):
        if (l[0] ==','):
            l = l[1:]
        if (l[-1] == ','):
            l = l[:-1]


        if (index % 2 == 0):
            for d in l.split(','):
                result.append(("'%s'" % d).replace(',', ' '))
        else:
            result.append(("'%s'" % l).replace(',', ' '))

    return ','.join(result)

def save_on_db(conn, dataset):

    
    c = conn.cursor()
    query = get_create_table_query(dataset[0])
    c.execute(query)
    conn.commit()

    for index, data in enumerate(dataset[1:]):
        try:
            query = "insert into dataset values (%s, %s);" % (index, to_string(data))
            c.execute(query)
        except:
            print ("Error on query: %s" % query)
    conn.commit()

def normalize_tables(conn):
    tables_to_normalize = ['DayOfWeek', 'PdDistrict', 'Resolution']
    
    c = conn.cursor()    
    for table in tables_to_normalize:
        query_create = "create table %s (id INTEGER PRIMARY KEY AUTOINCREMENT, value text);" % table
        c.execute(query_create)
        conn.commit()
        
        query_insert = "insert into {0} (value) select {0} from dataset group by {0};".format(table)
        c.execute(query_insert)
        conn.commit()

def create_new_dataset(conn, dataset_name):
    c = conn.cursor()
    new_dataset_name = 'new_%s_dataset' % dataset_name
    query_create = "CREATE TABLE %s AS SELECT * FROM dataset WHERE 0" % new_dataset_name
    c.execute(query_create)
    conn.commit()
    query = "insert into %s \
                select d.id, d.Dates, d.category, d.descript, days.id, dist.id, d.resolution, d.Address, d.X, d.Y from dataset d \
                inner join dayofweek days on days.value = d.DayOfWeek \
                inner join pddistrict dist on dist.value = d.PdDistrict" % new_dataset_name

    c.execute(query)
    conn.commit()
    return new_dataset_name

def export_to_csv(conn, output_db_name, columns):
    filename = '%s.csv' % output_db_name
    print ("export file %s" % filename)

    c = conn.cursor()
    cols = columns
    query = 'select {1} from {0}'.format(output_db_name, cols)
    result = c.execute(query)
    print query

    with open(filename, 'w') as f:
        f.write("%s\n" % cols)
        for r in result:
            f.write("%s\n" % ','.join(r))

def deg_to_dms(deg):
    d = int(deg)
    md = abs(deg - d) * 60
    m = int(md)
    sd = int((md - m) * 60)
    return [d, m, sd]

def expand_datetime(conn, db_name):
    new_db_name = '%s_expanded' % db_name
    
    c = conn.cursor()
    
    query_create = "CREATE TABLE %s AS SELECT * FROM dataset WHERE 0" % new_db_name
    c.execute(query_create)
    conn.commit()

    queries = 'ALTER TABLE {0} ADD COLUMN year text;\t \
                ALTER TABLE {0} ADD COLUMN month text;\t \
                ALTER TABLE {0} ADD COLUMN day text;\t \
                ALTER TABLE {0} ADD COLUMN hour text;\t \
                ALTER TABLE {0} ADD COLUMN minute text;\t \
                ALTER TABLE {0} ADD COLUMN minute_on_day text;\t \
                ALTER TABLE {0} ADD COLUMN day_on_year text;\t \
                ALTER TABLE {0} ADD COLUMN day_period text;\t \
                ALTER TABLE {0} ADD COLUMN month_period text;\t \
                ALTER TABLE {0} ADD COLUMN lon_degree text;\t \
                ALTER TABLE {0} ADD COLUMN lon_minute text;\t \
                ALTER TABLE {0} ADD COLUMN lon_second text;\t \
                ALTER TABLE {0} ADD COLUMN lat_degree text;\t \
                ALTER TABLE {0} ADD COLUMN lat_minute text;\t \
                ALTER TABLE {0} ADD COLUMN lat_second text;\t \
                '.format(new_db_name).split('\t')

    for query in queries:
        c.execute(query)
        conn.commit()
    
    query = 'select * from {0};'.format(db_name)
    table_data = c.execute(query).fetchall()

    for data in table_data:
        index = data[0]
        timestamp = data[1]
        lon = data[-2]
        lat = data[-1]
        lon_coor = deg_to_dms(float(lon))
        lat_coor = deg_to_dms(float(lat))

        date = timestamp.split(' ')
        d1 = date[0].split('-')
        d2 = date[1].split(':')

        minute_on_day = int(d2[0]) * 60 + int(d2[1])
        day_period = math.ceil(minute_on_day / (24 * 60 / 4))

        y, m, d = d1
        day_on_year = (datetime.date(int(y), int(m), int(d)) - datetime.date(int(y), 1, 1)).days
        month_period = math.ceil(int(d) / 7)
        values = [str(index)]
        for v in data[1:]:
            values.append("'%s'" % v)

        for v in d1:
            values.append("'%s'" % v)

        for v in d2[:-1]:
            values.append("'%s'" % v)

        values.append("'%s'" % minute_on_day)
        values.append("'%s'" % day_on_year)
        values.append("'%s'" % day_period)
        values.append("'%s'" % month_period)

        for v in lon_coor:
            values.append("'%s'" % v)
        for v in lat_coor:
            values.append("'%s'" % v)

        values = ','.join(values)

        query = 'insert into {0} values ({1});'.format(new_db_name, values)
        c.execute(query)

    conn.commit()
    return new_db_name

#!/usr/bin/python


def arguments(argv):
    columns = ''
    outputfile = OUTPUTFILENAME
    databasefile = DATABASENAME
    inputfile = INPUTFILENAME
    try:
        opts, args = getopt.getopt(argv,"hi:d:c:o:",["ifile=","dbfile=","columns=","ofile="])
    except getopt.GetoptError:
        print 'prepare_data.py -i <inputfile> -d <databasefile> -c "list,of,colums" -o <outputfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'prepare_data.py -i <inputfile> -d <databasefile> -c "list,of,colums" -o <outputfile>'
            sys.exit()
        elif opt in ("-c", "--columns"):
            columns = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-i", "--ifile"):
            columns = arg
        elif opt in ("-d", "--dbfile"):
            outputfile = arg

    return inputfile, databasefile, columns, outputfile

def clean_smallest_data(conn, db_name):
    print ("Deleting data witch category have minus than 10% of events")
    c = conn.cursor()
    query = 'select category, category_counter from ( \
		            select category, count(*) as category_counter from {0} group by category \
	            ) where category_counter < ( \
		            select (count(*) / count(distinct category) * 0.1) as ten_pc from {0} \
	            )  order by category_counter'.format(db_name)
    to_delete = c.execute(query).fetchall()
    print ("Categories to delete:")
    for t in to_delete:
        print ('\t%s' % t)

    query = 'delete from {0} where Category IN ( \
	            select category  from ( \
		            select category, count(*) as category_counter from {0} group by category \
	            ) where category_counter < ( \
		            select (count(*) / count(distinct category) * 0.1) as ten_pc from {0} \
	            )  order by category_counter \
             )'.format(db_name)


    c.execute(query)
    conn.commit()

def delete_none_resolution(conn, db_name):
    print ("Delete data witch resolution is NONE")

    query = "delete from {0} where Resolution = 'NONE'".format(db_name)

    c = conn.cursor()
    c.execute(query)
    conn.commit()

def clone_db(conn, old_db, new_db):
    print ('Clone {0} to {1}'.format(old_db, new_db))

    c = conn.cursor()

    query = 'CREATE TABLE {1} AS SELECT * FROM {0} WHERE 0'.format(old_db, new_db)
    c.execute(query)
    conn.commit()

    query = 'INSERT INTO {1} SELECT * FROM {0};'.format(old_db, new_db)
    c.execute(query)
    conn.commit()

if __name__ == "__main__":

    inputfile, databasefile, columns, outputfile = arguments(sys.argv[1:])
      
    try:
        os.remove(databasefile)
    except OSError:
        pass

    with open(inputfile) as f:
        dataset = f.readlines()
    
    conn = sqlite3.connect(databasefile)
    print 'connect to db'

    save_on_db(conn, dataset)
    print 'save on db'

    normalize_tables(conn)
    print 'normalize tables'
    
    new_db_name = create_new_dataset(conn, 'training')
    print 'new dataset created'
    
    expand_datetime(conn, new_db_name)    
    print 'new training dataset created'    

    db_name = 'new_training_dataset_expanded'
    clone_db(conn, db_name, "category_db")
    clone_db(conn, db_name, "resolution_db")

    clean_smallest_data(conn, "category_db")

    delete_none_resolution(conn, "resolution_db")

    export_to_csv(conn, "category_db", "%s, category" % columns)
    export_to_csv(conn, "resolution_db", "%s, resolution" % columns)

    print 'new training dataset exported'



