#!/bin/bash
# -*- ENCODING: UTF-8 -*-
T="$(date +%s)"

for i in $(eval echo "{$1..$2}")
do
	Rscript som.r category_db.csv category $i $i $3 > 'output/'$i'x'$i'_category_output_'$T'.txt'
done

