args <- commandArgs(trailingOnly = TRUE)
dataset_name <- args[1]
category_name <- args[2]
x <- as.integer(args[3])
y <- as.integer(args[4])
som_type <- args[5]

# load de library 
library("kohonen")
print("kohonen loaded")

library('caret') 
print("caret loaded")

# load dataset (http://archive.ics.uci.edu/ml/datasets/Wine)
dataset = read.csv(dataset_name) # the matrix wines (with columns names) store the data
print("training loaded")

training <- subset(dataset,year<2014)
testing <- subset(dataset,year>=2014)

if(category_name == 'category'){
	training.category <- training$category
	testing.category <- testing$category
	training$category <- NULL
	testing$category <- NULL
}else{
	training.category <- training$resolution
	testing.category <- testing$resolution
	training$resolution <- NULL
	testing$resolution <- NULL
}


#data normalization
training.sc <- scale(training)
testing.sc <- scale(testing)


print("training scaled")
 # seed for random inizialization
set.seed(7)
# training the som. somgrid (!!)
training.som <- som(data = training.sc, grid = somgrid(x,y,som_type))
print("som done")
summary(training.som)

#plots
print("Training progress")
changes_jpg_name = paste('images/',category_name,'_',x,'x',y,'_changes_',gsub("[ ]","_",Sys.time()),'.jpg',sep="")
jpeg(changes_jpg_name, width = 2048, height = 2048)
plot(training.som, type="changes")
dev.off()

codes_jpg_name = paste('images/',category_name,'_',x,'x',y,'_codes_',gsub("[ ]","_",Sys.time()),'.jpg',sep="")
jpeg(codes_jpg_name, width = 2048, height = 2048)
plot(training.som, main = "SF Crime Data")
dev.off()

count_jpg_name = paste('images/',category_name,'_',x,'x',y,'_count_',gsub("[ ]","_",Sys.time()),'.jpg',sep="")
jpeg(count_jpg_name,width = 2048, height = 2048)
plot(training.som, type="count")
dev.off()

coolBlueHotRed <- function(n, alpha = 1) {
  rainbow(n, end=4/6, alpha=alpha)[n:1]
}
quality_jpg_name = paste('images/',category_name,'_',x,'x',y,'_quality_',gsub("[ ]","_",Sys.time()),'.jpg',sep="")
jpeg(quality_jpg_name, width = 2048, height = 2048)
plot(training.som, type="quality", palette.name = coolBlueHotRed)
dev.off()

som.prediction<- predict (training.som, newdata= testing.sc, trainX = training.sc, 
                           trainY = classvec2classmat(training.category))

print("som prediction category")

predictions <- classmat2classvec(som.prediction$unit.predictions)

predictions.category_id <- as.numeric(factor(predictions, levels = unique(predictions)))
training.category_id <- as.numeric(factor(training.category, levels = unique(training.category)))

mapping_1_jpg_name = paste('images/',category_name,'_',x,'x',y,'_mapping_1_',gsub("[ ]","_",Sys.time()),'.jpg',sep="")
jpeg(mapping_1_jpg_name, width = 2048, height = 2048)
plot(training.som, type="mapping",
     labels = training.category_id, col =(training.category_id*5)+366,
     bgcol = colors()[(predictions.category_id*5)+366],
     main = "mapping plot category")
dev.off()

mapping_2_jpg_name = paste('images/',category_name,'_',x,'x',y,'_mapping_2_',gsub("[ ]","_",Sys.time()),'.jpg',sep="")
jpeg(mapping_2_jpg_name, width = 2048, height = 2048)
plot(training.som, type="mapping",
     bgcol = colors()[(predictions.category_id*5)+366],
     main = "mapping plot category")
dev.off()

# sacamos la confussion matrix.
confusionMatrix(som.prediction$prediction, testing.category)



