#!/bin/bash
# -*- ENCODING: UTF-8 -*-
hots=(13.79.162.58 40.69.37.149 40.69.39.155 40.85.136.15 52.169.190.149)
if [ "$#" -ge 3 ]; then	
	((a=$1))
	for server in ${hots[*]}; do
		echo "output from $server"
		script='./som_dsi/node_script.sh '$a' '$a' '$3
		echo $script
		(ssh -i azure_dsi vagrant@$server tmux new-session -d -s my_session '"'$script'"' ;  echo End $server) &
		((a+=$2))
	done
	wait
	echo All subshells finished
else
	echo 'Error: ./ssh_run_script.sh ini inc Type'
fi